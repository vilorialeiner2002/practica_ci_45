<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Select Best Match            Live Te_27b76f</name>
   <tag></tag>
   <elementGuidId>42eff72c-d85b-4dc0-bd35-bf77675a0ccf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='testingTypeSelection']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;testingTypeSelection&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>76966c02-4181-450d-9437-103bdb5e5125</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>testingTypeSelection</value>
      <webElementGuid>61cad16e-eabc-40f1-bb1b-117eac69d2a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>free-trial-entry</value>
      <webElementGuid>2a472577-b015-49a6-afa2-9a6c7cd66d23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>dc3fe26f-7081-417e-999f-36c683ceb4e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Select Best Match
            Live Testing (Manual Testing)
            Selenium Testing (Automated Testing)
            Record &amp; Replay (Codeless Automation)
            Full-Page Screenshots (Visual Testing)
         </value>
      <webElementGuid>95a98b71-e96b-4e80-8fda-4266860bc31f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;signup-form&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col s12&quot;]/span[@class=&quot;input-wrap&quot;]/select[@class=&quot;free-trial-entry&quot;]</value>
      <webElementGuid>b5ca7516-2e24-47de-ac55-7b792b7ce160</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='testingTypeSelection']</value>
      <webElementGuid>a43f4b81-9960-4b65-86d9-3777739cbc7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='signup-form']/div[8]/div/span/select</value>
      <webElementGuid>98c5ab55-a993-4ba5-afcb-8018d476c6c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='What Type of Testing Do You Want to Accomplish In Your Trial?*'])[1]/following::select[1]</value>
      <webElementGuid>96b1fbc6-eade-4bff-85d1-6781b28b8aee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Job Role is required'])[1]/following::select[2]</value>
      <webElementGuid>bba6160f-1f12-4899-91be-6463420c60a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please Complete Form'])[1]/preceding::select[1]</value>
      <webElementGuid>4956c9c5-74a3-4f2e-ac83-e7738670f788</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create your free account'])[1]/preceding::select[1]</value>
      <webElementGuid>5016af6c-5a69-4d8b-a1ea-8b6cddb04799</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/select</value>
      <webElementGuid>c06e54b4-e8e5-4d75-baae-1cb3cbb8a331</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'testingTypeSelection' and (text() = '
            Select Best Match
            Live Testing (Manual Testing)
            Selenium Testing (Automated Testing)
            Record &amp; Replay (Codeless Automation)
            Full-Page Screenshots (Visual Testing)
         ' or . = '
            Select Best Match
            Live Testing (Manual Testing)
            Selenium Testing (Automated Testing)
            Record &amp; Replay (Codeless Automation)
            Full-Page Screenshots (Visual Testing)
         ')]</value>
      <webElementGuid>425c9d0b-dae5-434c-a859-08b54a2e6ed7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
