<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Select Best MatchStudentDesignerDeve_b45b97</name>
   <tag></tag>
   <elementGuidId>31316f1d-98a9-4e0d-8b4c-3f8f013ad740</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='webtitle']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;webtitle&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>4bb4aaf1-d6fe-48a0-b37d-5bc93a75150c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>webtitle</value>
      <webElementGuid>01338694-1748-44b9-bd6d-47aacce237ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>free-trial-entry</value>
      <webElementGuid>0300f316-4cdf-493c-8c38-5c81184df252</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>149c2fbd-0adf-46fa-8c95-04e0d70caa05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
									Select Best Match
									Student
									Designer
									Developer
									DevOps
									Manager / Director
									Project Manager
									QA
									Other
								</value>
      <webElementGuid>9426663e-cd92-4482-a685-bed48a4484bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;signup-form&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col s12 webtitle&quot;]/div[@class=&quot;input-wrap&quot;]/select[@class=&quot;free-trial-entry&quot;]</value>
      <webElementGuid>d8ec1b97-af15-49ee-8595-d74bf0e0897b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='webtitle']</value>
      <webElementGuid>bed80e5f-7bb8-43b2-b7a3-90c614559277</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='signup-form']/div[7]/div/div/select</value>
      <webElementGuid>59554f56-be91-4a47-be54-5d16934a9c15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Job Role is required'])[1]/following::select[1]</value>
      <webElementGuid>4cd5f2be-5898-45a8-82bb-5a84a6857785</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='What Best Describes You?*'])[1]/following::select[1]</value>
      <webElementGuid>4dc43108-e829-412d-9aa3-34eed6129966</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='What Type of Testing Do You Want to Accomplish In Your Trial?*'])[1]/preceding::select[1]</value>
      <webElementGuid>72f76f5b-2342-4578-9ed7-e20107e80131</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please Complete Form'])[1]/preceding::select[2]</value>
      <webElementGuid>d8259adb-26a3-4263-9ef2-43c341d600a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>9e0b1219-cb6d-4a85-86ce-0da6c3072b1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'webtitle' and (text() = '
									Select Best Match
									Student
									Designer
									Developer
									DevOps
									Manager / Director
									Project Manager
									QA
									Other
								' or . = '
									Select Best Match
									Student
									Designer
									Developer
									DevOps
									Manager / Director
									Project Manager
									QA
									Other
								')]</value>
      <webElementGuid>38bc306f-f21a-4503-b5c2-4a396a6ed342</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
