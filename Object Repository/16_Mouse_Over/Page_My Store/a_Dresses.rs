<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Dresses</name>
   <tag></tag>
   <elementGuidId>6b5ee993-3786-4790-a587-c0f2bfc5c03a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.sfHover > a.sf-with-ul</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block_top_menu']/ul/li[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>34144597-8044-46e2-8521-279b8c6a688c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_category=8&amp;controller=category</value>
      <webElementGuid>274f9eb7-b5b2-4404-b98b-879e9120fda3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Dresses</value>
      <webElementGuid>1aea8ea1-7798-4fe7-a01c-52ce201579b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sf-with-ul</value>
      <webElementGuid>cf8c05ab-fd7c-468b-a767-1137252318d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Dresses</value>
      <webElementGuid>17d8bbaf-c131-4f6d-8d20-e0a83502807b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block_top_menu&quot;)/ul[@class=&quot;sf-menu clearfix menu-content sf-js-enabled sf-arrows&quot;]/li[@class=&quot;sfHover&quot;]/a[@class=&quot;sf-with-ul&quot;]</value>
      <webElementGuid>248e592d-4d4b-4123-85e4-596fbedc61b8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block_top_menu']/ul/li[2]/a</value>
      <webElementGuid>3892086d-d98c-4459-bd19-12d9130fd106</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Dresses')])[5]</value>
      <webElementGuid>c0ad73fc-4198-48ab-964c-b7bf679da321</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Summer Dresses'])[1]/following::a[1]</value>
      <webElementGuid>4be58b05-8197-45d4-af8f-d225ac3b1050</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Evening Dresses'])[1]/following::a[2]</value>
      <webElementGuid>63d745da-2f12-4a6a-abbe-5f78b3f11e12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Casual Dresses'])[2]/preceding::a[1]</value>
      <webElementGuid>bb5cac62-8edf-4f91-9a6e-caba0313eed9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Evening Dresses'])[2]/preceding::a[2]</value>
      <webElementGuid>253d1eb9-9e14-4a24-a92c-8b0877567bdc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[@href='http://automationpractice.com/index.php?id_category=8&amp;controller=category'])[2]</value>
      <webElementGuid>5f30afec-ae94-4059-b4fd-31c49f083fb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a</value>
      <webElementGuid>24b63514-6fd8-45db-8474-b2cacbe74850</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_category=8&amp;controller=category' and @title = 'Dresses' and (text() = 'Dresses' or . = 'Dresses')]</value>
      <webElementGuid>b52ecb9f-873d-4ac9-8bbc-fffd2ddeee24</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
