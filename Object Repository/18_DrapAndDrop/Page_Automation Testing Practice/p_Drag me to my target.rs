<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Drag me to my target</name>
   <tag></tag>
   <elementGuidId>b5ef9380-8624-4e81-942e-6c50e0632593</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#draggable > p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='draggable']/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>e2ddf0a4-766b-4599-a96e-6e669bc16715</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Drag me to my target</value>
      <webElementGuid>accda711-f066-4442-91cf-2ed544b4dddb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;draggable&quot;)/p[1]</value>
      <webElementGuid>3c9a75c0-1bd0-4b27-875b-c280310b54f7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='draggable']/p</value>
      <webElementGuid>bbee40ab-9c4d-48c8-9095-4e08d3353176</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drag and Drop'])[1]/following::p[1]</value>
      <webElementGuid>7a33c577-349c-4c38-98ee-ba7ed79d8fc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy Text'])[1]/following::p[2]</value>
      <webElementGuid>121b40b3-ce3e-427d-9308-71d348933d55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drag and Drop Images'])[1]/preceding::p[2]</value>
      <webElementGuid>2af506ed-19b5-4c58-9dce-4091fa314d48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mr John'])[1]/preceding::p[2]</value>
      <webElementGuid>d923c87a-32c0-4773-9a4b-312c23793c24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Drag me to my target']/parent::*</value>
      <webElementGuid>9b7a17ce-bf62-4eba-9b98-997e3be9dfad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/p</value>
      <webElementGuid>8dbaaba1-8e13-47b5-aaf3-d41636d37ade</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Drag me to my target' or . = 'Drag me to my target')]</value>
      <webElementGuid>cde7188e-c390-4a6b-8560-f92bfc424476</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
