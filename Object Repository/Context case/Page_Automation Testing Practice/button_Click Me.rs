<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Click Me</name>
   <tag></tag>
   <elementGuidId>ac235065-bfd4-4954-8f61-743b88f9be64</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@onclick='myFunction()']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>53cd1e42-f605-4199-ac0e-ea13eee15b98</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>myFunction()</value>
      <webElementGuid>575f55ff-bdf5-43ea-ab30-18422e3911e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Click Me</value>
      <webElementGuid>3a0fbec5-fb45-4bc7-a765-dbb845989767</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;HTML9&quot;)/div[@class=&quot;widget-content&quot;]/button[1]</value>
      <webElementGuid>8e13b7cc-eb6d-4159-ac6e-3d031b924c65</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@onclick='myFunction()']</value>
      <webElementGuid>058be05e-5791-4fd7-8b4a-80ed7e8bed90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='HTML9']/div/button</value>
      <webElementGuid>d12711f4-8012-4d76-8610-6e0f26a7ee7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alert'])[1]/following::button[1]</value>
      <webElementGuid>21a653e9-fbb2-4516-8bb0-8b671ae09a96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search results'])[1]/following::button[1]</value>
      <webElementGuid>3cb3a9e1-800c-4a90-8afe-c3f85c51b65a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date Picker'])[1]/preceding::button[1]</value>
      <webElementGuid>79870836-2a3a-49b5-90b6-c6312d1d4d08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select menu'])[1]/preceding::button[1]</value>
      <webElementGuid>c9940c25-291f-493d-910d-e66eb17c1dd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Click Me']/parent::*</value>
      <webElementGuid>cf43850c-ef12-464d-9d20-909162582025</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>9439c119-2b52-46ba-b9c8-60a5d6ceebc3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Click Me' or . = 'Click Me')]</value>
      <webElementGuid>f9b30542-5b84-493c-9fab-f916b228afc1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
