<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Double ClickField1 Field2 Copy TextDoub_c9d6e0</name>
   <tag></tag>
   <elementGuidId>514356bf-7578-4d94-a6ad-e97f10d5cf07</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.column-right-inner</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='marry@email.com'])[1]/following::div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7dfbf464-bafd-42d0-a972-739e5aee940c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>column-right-inner</value>
      <webElementGuid>4b10bf78-9c76-48ce-9674-bde66e532018</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>


Double Click





Field1: 
Field2: 

Copy Text

Double click on button, the text from Field1 will be copied into Field2.


function myFunction1() {
    document.getElementById(&quot;field2&quot;).value = document.getElementById(&quot;field1&quot;).value;
}







Drag and Drop




  
  
  #draggable { width: 100px; height: 100px; padding: 0.5em; float: left; margin: 10px 10px 10px 0; }
  #droppable { width: 150px; height: 150px; padding: 0.5em; float: left; margin: 10px; }
  
  
  
  
  $( function() {
    $( &quot;#draggable&quot; ).draggable();
    $( &quot;#droppable&quot; ).droppable({
      drop: function( event, ui ) {
        $( this )
          .addClass( &quot;ui-state-highlight&quot; )
          .find( &quot;p&quot; )
            .html( &quot;Dropped!&quot; );
      }
    });
  } );
  


 

  Drag me to my target

 

  Drop here

  




Drag and Drop Images




  
  
    
  
  
  #gallery { float: left; width: 65%; min-height: 12em; }
  .gallery.custom-state-active { background: #eee; }
  .gallery li { float: left; width: 96px; padding: 0.4em; margin: 0 0.4em 0.4em 0; text-align: center; }
  .gallery li h5 { margin: 0 0 0.4em; cursor: move; }
  .gallery li a { float: right; }
  .gallery li a.ui-icon-zoomin { float: left; }
  .gallery li img { width: 100%; cursor: move; }
 
  #trash { float: right; width: 32%; min-height: 18em; padding: 1%; }
  #trash h4 { line-height: 16px; margin: 0 0 0.4em; }
  #trash h4 .ui-icon { float: left; }
  #trash .gallery h5 { display: none; }
  
  
  
  
  $( function() {
 
    // There's the gallery and the trash
    var $gallery = $( &quot;#gallery&quot; ),
      $trash = $( &quot;#trash&quot; );
 
    // Let the gallery items be draggable
    $( &quot;li&quot;, $gallery ).draggable({
      cancel: &quot;a.ui-icon&quot;, // clicking an icon won't initiate dragging
      revert: &quot;invalid&quot;, // when not dropped, the item will revert back to its initial position
      containment: &quot;document&quot;,
      helper: &quot;clone&quot;,
      cursor: &quot;move&quot;
    });
 
    // Let the trash be droppable, accepting the gallery items
    $trash.droppable({
      accept: &quot;#gallery > li&quot;,
      classes: {
        &quot;ui-droppable-active&quot;: &quot;ui-state-highlight&quot;
      },
      drop: function( event, ui ) {
        deleteImage( ui.draggable );
      }
    });
 
    // Let the gallery be droppable as well, accepting items from the trash
    $gallery.droppable({
      accept: &quot;#trash li&quot;,
      classes: {
        &quot;ui-droppable-active&quot;: &quot;custom-state-active&quot;
      },
      drop: function( event, ui ) {
        recycleImage( ui.draggable );
      }
    });
 
    // Image deletion function
    var recycle_icon = &quot;&lt;a href='link/to/recycle/script/when/we/have/js/off' title='Recycle this image' class='ui-icon ui-icon-refresh'>Recycle image&lt;/a>&quot;;
    function deleteImage( $item ) {
      $item.fadeOut(function() {
        var $list = $( &quot;ul&quot;, $trash ).length ?
          $( &quot;ul&quot;, $trash ) :
          $( &quot;&lt;ul class='gallery ui-helper-reset'/>&quot; ).appendTo( $trash );
 
        $item.find( &quot;a.ui-icon-trash&quot; ).remove();
        $item.append( recycle_icon ).appendTo( $list ).fadeIn(function() {
          $item
            .animate({ width: &quot;48px&quot; })
            .find( &quot;img&quot; )
              .animate({ height: &quot;36px&quot; });
        });
      });
    }
 
    // Image recycle function
    var trash_icon = &quot;&lt;a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='ui-icon ui-icon-trash'>Delete image&lt;/a>&quot;;
    function recycleImage( $item ) {
      $item.fadeOut(function() {
        $item
          .find( &quot;a.ui-icon-refresh&quot; )
            .remove()
          .end()
          .css( &quot;width&quot;, &quot;96px&quot;)
          .append( trash_icon )
          .find( &quot;img&quot; )
            .css( &quot;height&quot;, &quot;72px&quot; )
          .end()
          .appendTo( $gallery )
          .fadeIn();
      });
    }
 
    // Image preview function, demonstrating the ui.dialog used as a modal window
    function viewLargerImage( $link ) {
      var src = $link.attr( &quot;href&quot; ),
        title = $link.siblings( &quot;img&quot; ).attr( &quot;alt&quot; ),
        $modal = $( &quot;img[src$='&quot; + src + &quot;']&quot; );
 
      if ( $modal.length ) {
        $modal.dialog( &quot;open&quot; );
      } else {
        var img = $( &quot;&lt;img alt='&quot; + title + &quot;' width='384' height='288' style='display: none; padding: 8px;' />&quot; )
          .attr( &quot;src&quot;, src ).appendTo( &quot;body&quot; );
        setTimeout(function() {
          img.dialog({
            title: title,
            width: 400,
            modal: true
          });
        }, 1 );
      }
    }
 
    // Resolve the icons behavior with event delegation
    $( &quot;ul.gallery > li&quot; ).on( &quot;click&quot;, function( event ) {
      var $item = $( this ),
        $target = $( event.target );
 
      if ( $target.is( &quot;a.ui-icon-trash&quot; ) ) {
        deleteImage( $item );
      } else if ( $target.is( &quot;a.ui-icon-zoomin&quot; ) ) {
        viewLargerImage( $target );
      } else if ( $target.is( &quot;a.ui-icon-refresh&quot; ) ) {
        recycleImage( $item );
      }
 
      return false;
    });
  } );
  


 

 

  
    Mr John
    
 
  
    Mary
    
  

 

  Trash Trash

 

 
 





Slider




  
  
  
  
  $( function() {
    $( &quot;#slider&quot; ).slider();
  } );
  


 

 
 





Resizable




  
  
  
  
  
  
  #resizable { width: 150px; height: 150px; padding: 0.5em; }
  #resizable h3 { text-align: center; margin: 0; }
  
  
  
  
  $( function() {
    $( &quot;#resizable&quot; ).resizable();
  } );
  


 

  Resizable

 
 






</value>
      <webElementGuid>7d121b4a-7862-4696-9a42-765abdf52ff8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;v2&quot;]/body[@class=&quot;variant-wide&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;content-outer&quot;]/div[@class=&quot;fauxborder-left content-fauxborder-left&quot;]/div[@class=&quot;content-inner&quot;]/div[@class=&quot;main-outer&quot;]/div[@class=&quot;fauxborder-left main-fauxborder-left&quot;]/div[@class=&quot;region-inner main-inner&quot;]/div[@class=&quot;columns fauxcolumns&quot;]/div[@class=&quot;columns-inner&quot;]/div[@class=&quot;column-right-outer&quot;]/div[@class=&quot;column-right-inner&quot;]</value>
      <webElementGuid>95235d7a-bae9-4177-9e59-e3940e5511ea</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='marry@email.com'])[1]/following::div[3]</value>
      <webElementGuid>93ad08a8-f671-49e6-92a7-d41b4b835c62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Application Developer'])[1]/following::div[3]</value>
      <webElementGuid>a0de54ff-21c2-485e-b46e-2eee4603d90c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[3]/div</value>
      <webElementGuid>980f3bd8-283c-4d1b-abe4-fd1d05cf3293</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;


Double Click





Field1: 
Field2: 

Copy Text

Double click on button, the text from Field1 will be copied into Field2.


function myFunction1() {
    document.getElementById(&quot;field2&quot;).value = document.getElementById(&quot;field1&quot;).value;
}







Drag and Drop




  
  
  #draggable { width: 100px; height: 100px; padding: 0.5em; float: left; margin: 10px 10px 10px 0; }
  #droppable { width: 150px; height: 150px; padding: 0.5em; float: left; margin: 10px; }
  
  
  
  
  $( function() {
    $( &quot;#draggable&quot; ).draggable();
    $( &quot;#droppable&quot; ).droppable({
      drop: function( event, ui ) {
        $( this )
          .addClass( &quot;ui-state-highlight&quot; )
          .find( &quot;p&quot; )
            .html( &quot;Dropped!&quot; );
      }
    });
  } );
  


 

  Drag me to my target

 

  Drop here

  




Drag and Drop Images




  
  
    
  
  
  #gallery { float: left; width: 65%; min-height: 12em; }
  .gallery.custom-state-active { background: #eee; }
  .gallery li { float: left; width: 96px; padding: 0.4em; margin: 0 0.4em 0.4em 0; text-align: center; }
  .gallery li h5 { margin: 0 0 0.4em; cursor: move; }
  .gallery li a { float: right; }
  .gallery li a.ui-icon-zoomin { float: left; }
  .gallery li img { width: 100%; cursor: move; }
 
  #trash { float: right; width: 32%; min-height: 18em; padding: 1%; }
  #trash h4 { line-height: 16px; margin: 0 0 0.4em; }
  #trash h4 .ui-icon { float: left; }
  #trash .gallery h5 { display: none; }
  
  
  
  
  $( function() {
 
    // There&quot; , &quot;'&quot; , &quot;s the gallery and the trash
    var $gallery = $( &quot;#gallery&quot; ),
      $trash = $( &quot;#trash&quot; );
 
    // Let the gallery items be draggable
    $( &quot;li&quot;, $gallery ).draggable({
      cancel: &quot;a.ui-icon&quot;, // clicking an icon won&quot; , &quot;'&quot; , &quot;t initiate dragging
      revert: &quot;invalid&quot;, // when not dropped, the item will revert back to its initial position
      containment: &quot;document&quot;,
      helper: &quot;clone&quot;,
      cursor: &quot;move&quot;
    });
 
    // Let the trash be droppable, accepting the gallery items
    $trash.droppable({
      accept: &quot;#gallery > li&quot;,
      classes: {
        &quot;ui-droppable-active&quot;: &quot;ui-state-highlight&quot;
      },
      drop: function( event, ui ) {
        deleteImage( ui.draggable );
      }
    });
 
    // Let the gallery be droppable as well, accepting items from the trash
    $gallery.droppable({
      accept: &quot;#trash li&quot;,
      classes: {
        &quot;ui-droppable-active&quot;: &quot;custom-state-active&quot;
      },
      drop: function( event, ui ) {
        recycleImage( ui.draggable );
      }
    });
 
    // Image deletion function
    var recycle_icon = &quot;&lt;a href=&quot; , &quot;'&quot; , &quot;link/to/recycle/script/when/we/have/js/off&quot; , &quot;'&quot; , &quot; title=&quot; , &quot;'&quot; , &quot;Recycle this image&quot; , &quot;'&quot; , &quot; class=&quot; , &quot;'&quot; , &quot;ui-icon ui-icon-refresh&quot; , &quot;'&quot; , &quot;>Recycle image&lt;/a>&quot;;
    function deleteImage( $item ) {
      $item.fadeOut(function() {
        var $list = $( &quot;ul&quot;, $trash ).length ?
          $( &quot;ul&quot;, $trash ) :
          $( &quot;&lt;ul class=&quot; , &quot;'&quot; , &quot;gallery ui-helper-reset&quot; , &quot;'&quot; , &quot;/>&quot; ).appendTo( $trash );
 
        $item.find( &quot;a.ui-icon-trash&quot; ).remove();
        $item.append( recycle_icon ).appendTo( $list ).fadeIn(function() {
          $item
            .animate({ width: &quot;48px&quot; })
            .find( &quot;img&quot; )
              .animate({ height: &quot;36px&quot; });
        });
      });
    }
 
    // Image recycle function
    var trash_icon = &quot;&lt;a href=&quot; , &quot;'&quot; , &quot;link/to/trash/script/when/we/have/js/off&quot; , &quot;'&quot; , &quot; title=&quot; , &quot;'&quot; , &quot;Delete this image&quot; , &quot;'&quot; , &quot; class=&quot; , &quot;'&quot; , &quot;ui-icon ui-icon-trash&quot; , &quot;'&quot; , &quot;>Delete image&lt;/a>&quot;;
    function recycleImage( $item ) {
      $item.fadeOut(function() {
        $item
          .find( &quot;a.ui-icon-refresh&quot; )
            .remove()
          .end()
          .css( &quot;width&quot;, &quot;96px&quot;)
          .append( trash_icon )
          .find( &quot;img&quot; )
            .css( &quot;height&quot;, &quot;72px&quot; )
          .end()
          .appendTo( $gallery )
          .fadeIn();
      });
    }
 
    // Image preview function, demonstrating the ui.dialog used as a modal window
    function viewLargerImage( $link ) {
      var src = $link.attr( &quot;href&quot; ),
        title = $link.siblings( &quot;img&quot; ).attr( &quot;alt&quot; ),
        $modal = $( &quot;img[src$=&quot; , &quot;'&quot; , &quot;&quot; + src + &quot;&quot; , &quot;'&quot; , &quot;]&quot; );
 
      if ( $modal.length ) {
        $modal.dialog( &quot;open&quot; );
      } else {
        var img = $( &quot;&lt;img alt=&quot; , &quot;'&quot; , &quot;&quot; + title + &quot;&quot; , &quot;'&quot; , &quot; width=&quot; , &quot;'&quot; , &quot;384&quot; , &quot;'&quot; , &quot; height=&quot; , &quot;'&quot; , &quot;288&quot; , &quot;'&quot; , &quot; style=&quot; , &quot;'&quot; , &quot;display: none; padding: 8px;&quot; , &quot;'&quot; , &quot; />&quot; )
          .attr( &quot;src&quot;, src ).appendTo( &quot;body&quot; );
        setTimeout(function() {
          img.dialog({
            title: title,
            width: 400,
            modal: true
          });
        }, 1 );
      }
    }
 
    // Resolve the icons behavior with event delegation
    $( &quot;ul.gallery > li&quot; ).on( &quot;click&quot;, function( event ) {
      var $item = $( this ),
        $target = $( event.target );
 
      if ( $target.is( &quot;a.ui-icon-trash&quot; ) ) {
        deleteImage( $item );
      } else if ( $target.is( &quot;a.ui-icon-zoomin&quot; ) ) {
        viewLargerImage( $target );
      } else if ( $target.is( &quot;a.ui-icon-refresh&quot; ) ) {
        recycleImage( $item );
      }
 
      return false;
    });
  } );
  


 

 

  
    Mr John
    
 
  
    Mary
    
  

 

  Trash Trash

 

 
 





Slider




  
  
  
  
  $( function() {
    $( &quot;#slider&quot; ).slider();
  } );
  


 

 
 





Resizable




  
  
  
  
  
  
  #resizable { width: 150px; height: 150px; padding: 0.5em; }
  #resizable h3 { text-align: center; margin: 0; }
  
  
  
  
  $( function() {
    $( &quot;#resizable&quot; ).resizable();
  } );
  


 

  Resizable

 
 






&quot;) or . = concat(&quot;


Double Click





Field1: 
Field2: 

Copy Text

Double click on button, the text from Field1 will be copied into Field2.


function myFunction1() {
    document.getElementById(&quot;field2&quot;).value = document.getElementById(&quot;field1&quot;).value;
}







Drag and Drop




  
  
  #draggable { width: 100px; height: 100px; padding: 0.5em; float: left; margin: 10px 10px 10px 0; }
  #droppable { width: 150px; height: 150px; padding: 0.5em; float: left; margin: 10px; }
  
  
  
  
  $( function() {
    $( &quot;#draggable&quot; ).draggable();
    $( &quot;#droppable&quot; ).droppable({
      drop: function( event, ui ) {
        $( this )
          .addClass( &quot;ui-state-highlight&quot; )
          .find( &quot;p&quot; )
            .html( &quot;Dropped!&quot; );
      }
    });
  } );
  


 

  Drag me to my target

 

  Drop here

  




Drag and Drop Images




  
  
    
  
  
  #gallery { float: left; width: 65%; min-height: 12em; }
  .gallery.custom-state-active { background: #eee; }
  .gallery li { float: left; width: 96px; padding: 0.4em; margin: 0 0.4em 0.4em 0; text-align: center; }
  .gallery li h5 { margin: 0 0 0.4em; cursor: move; }
  .gallery li a { float: right; }
  .gallery li a.ui-icon-zoomin { float: left; }
  .gallery li img { width: 100%; cursor: move; }
 
  #trash { float: right; width: 32%; min-height: 18em; padding: 1%; }
  #trash h4 { line-height: 16px; margin: 0 0 0.4em; }
  #trash h4 .ui-icon { float: left; }
  #trash .gallery h5 { display: none; }
  
  
  
  
  $( function() {
 
    // There&quot; , &quot;'&quot; , &quot;s the gallery and the trash
    var $gallery = $( &quot;#gallery&quot; ),
      $trash = $( &quot;#trash&quot; );
 
    // Let the gallery items be draggable
    $( &quot;li&quot;, $gallery ).draggable({
      cancel: &quot;a.ui-icon&quot;, // clicking an icon won&quot; , &quot;'&quot; , &quot;t initiate dragging
      revert: &quot;invalid&quot;, // when not dropped, the item will revert back to its initial position
      containment: &quot;document&quot;,
      helper: &quot;clone&quot;,
      cursor: &quot;move&quot;
    });
 
    // Let the trash be droppable, accepting the gallery items
    $trash.droppable({
      accept: &quot;#gallery > li&quot;,
      classes: {
        &quot;ui-droppable-active&quot;: &quot;ui-state-highlight&quot;
      },
      drop: function( event, ui ) {
        deleteImage( ui.draggable );
      }
    });
 
    // Let the gallery be droppable as well, accepting items from the trash
    $gallery.droppable({
      accept: &quot;#trash li&quot;,
      classes: {
        &quot;ui-droppable-active&quot;: &quot;custom-state-active&quot;
      },
      drop: function( event, ui ) {
        recycleImage( ui.draggable );
      }
    });
 
    // Image deletion function
    var recycle_icon = &quot;&lt;a href=&quot; , &quot;'&quot; , &quot;link/to/recycle/script/when/we/have/js/off&quot; , &quot;'&quot; , &quot; title=&quot; , &quot;'&quot; , &quot;Recycle this image&quot; , &quot;'&quot; , &quot; class=&quot; , &quot;'&quot; , &quot;ui-icon ui-icon-refresh&quot; , &quot;'&quot; , &quot;>Recycle image&lt;/a>&quot;;
    function deleteImage( $item ) {
      $item.fadeOut(function() {
        var $list = $( &quot;ul&quot;, $trash ).length ?
          $( &quot;ul&quot;, $trash ) :
          $( &quot;&lt;ul class=&quot; , &quot;'&quot; , &quot;gallery ui-helper-reset&quot; , &quot;'&quot; , &quot;/>&quot; ).appendTo( $trash );
 
        $item.find( &quot;a.ui-icon-trash&quot; ).remove();
        $item.append( recycle_icon ).appendTo( $list ).fadeIn(function() {
          $item
            .animate({ width: &quot;48px&quot; })
            .find( &quot;img&quot; )
              .animate({ height: &quot;36px&quot; });
        });
      });
    }
 
    // Image recycle function
    var trash_icon = &quot;&lt;a href=&quot; , &quot;'&quot; , &quot;link/to/trash/script/when/we/have/js/off&quot; , &quot;'&quot; , &quot; title=&quot; , &quot;'&quot; , &quot;Delete this image&quot; , &quot;'&quot; , &quot; class=&quot; , &quot;'&quot; , &quot;ui-icon ui-icon-trash&quot; , &quot;'&quot; , &quot;>Delete image&lt;/a>&quot;;
    function recycleImage( $item ) {
      $item.fadeOut(function() {
        $item
          .find( &quot;a.ui-icon-refresh&quot; )
            .remove()
          .end()
          .css( &quot;width&quot;, &quot;96px&quot;)
          .append( trash_icon )
          .find( &quot;img&quot; )
            .css( &quot;height&quot;, &quot;72px&quot; )
          .end()
          .appendTo( $gallery )
          .fadeIn();
      });
    }
 
    // Image preview function, demonstrating the ui.dialog used as a modal window
    function viewLargerImage( $link ) {
      var src = $link.attr( &quot;href&quot; ),
        title = $link.siblings( &quot;img&quot; ).attr( &quot;alt&quot; ),
        $modal = $( &quot;img[src$=&quot; , &quot;'&quot; , &quot;&quot; + src + &quot;&quot; , &quot;'&quot; , &quot;]&quot; );
 
      if ( $modal.length ) {
        $modal.dialog( &quot;open&quot; );
      } else {
        var img = $( &quot;&lt;img alt=&quot; , &quot;'&quot; , &quot;&quot; + title + &quot;&quot; , &quot;'&quot; , &quot; width=&quot; , &quot;'&quot; , &quot;384&quot; , &quot;'&quot; , &quot; height=&quot; , &quot;'&quot; , &quot;288&quot; , &quot;'&quot; , &quot; style=&quot; , &quot;'&quot; , &quot;display: none; padding: 8px;&quot; , &quot;'&quot; , &quot; />&quot; )
          .attr( &quot;src&quot;, src ).appendTo( &quot;body&quot; );
        setTimeout(function() {
          img.dialog({
            title: title,
            width: 400,
            modal: true
          });
        }, 1 );
      }
    }
 
    // Resolve the icons behavior with event delegation
    $( &quot;ul.gallery > li&quot; ).on( &quot;click&quot;, function( event ) {
      var $item = $( this ),
        $target = $( event.target );
 
      if ( $target.is( &quot;a.ui-icon-trash&quot; ) ) {
        deleteImage( $item );
      } else if ( $target.is( &quot;a.ui-icon-zoomin&quot; ) ) {
        viewLargerImage( $target );
      } else if ( $target.is( &quot;a.ui-icon-refresh&quot; ) ) {
        recycleImage( $item );
      }
 
      return false;
    });
  } );
  


 

 

  
    Mr John
    
 
  
    Mary
    
  

 

  Trash Trash

 

 
 





Slider




  
  
  
  
  $( function() {
    $( &quot;#slider&quot; ).slider();
  } );
  


 

 
 





Resizable




  
  
  
  
  
  
  #resizable { width: 150px; height: 150px; padding: 0.5em; }
  #resizable h3 { text-align: center; margin: 0; }
  
  
  
  
  $( function() {
    $( &quot;#resizable&quot; ).resizable();
  } );
  


 

  Resizable

 
 






&quot;))]</value>
      <webElementGuid>d5a15197-b2ca-413d-8855-d4de67e19cb5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
