<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>fieldset_Select a Product    GoogleYahooIph_b9b927</name>
   <tag></tag>
   <elementGuidId>f970a2ce-559f-4feb-bac0-6d819f4df40c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='HTML6']/div/form/fieldset[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>fieldset</value>
      <webElementGuid>ad0be2c3-7d63-4a43-84de-b1fe7c193a30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	Select a Product:
    
		Google
		Yahoo
		Iphone
		Bing
	
	
	Select a Animal:
    
		Cat
		Baby Cat
		Big Baby Cat
		Avatar
	
	
 </value>
      <webElementGuid>562bc702-7cdb-42e6-932c-7c76b9ce3e57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;HTML6&quot;)/div[@class=&quot;widget-content&quot;]/form[1]/fieldset[2]</value>
      <webElementGuid>4919abf0-34bc-4db6-a758-3c566f03beaa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='HTML6']/div/form/fieldset[2]</value>
      <webElementGuid>576f44b7-dea6-443c-b800-7b4afa75a9bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select a number'])[1]/following::fieldset[1]</value>
      <webElementGuid>e38fb966-9630-45db-adf2-006a1724610c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select a file'])[1]/following::fieldset[1]</value>
      <webElementGuid>bafd54b0-9790-4269-a536-cdb863e6151b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[2]</value>
      <webElementGuid>20c389ba-bd7d-46b9-90ff-5d1098da529c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//fieldset[(text() = '
	Select a Product:
    
		Google
		Yahoo
		Iphone
		Bing
	
	
	Select a Animal:
    
		Cat
		Baby Cat
		Big Baby Cat
		Avatar
	
	
 ' or . = '
	Select a Product:
    
		Google
		Yahoo
		Iphone
		Bing
	
	
	Select a Animal:
    
		Cat
		Baby Cat
		Big Baby Cat
		Avatar
	
	
 ')]</value>
      <webElementGuid>3d63f847-a51a-4a0a-a99b-140986c3e9a3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
