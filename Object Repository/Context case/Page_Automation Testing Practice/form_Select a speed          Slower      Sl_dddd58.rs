<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_Select a speed          Slower      Sl_dddd58</name>
   <tag></tag>
   <elementGuidId>67513f8b-babd-4339-8dbb-ffbea88cdb5b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@action='#']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.widget-content > form</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>4c7760b3-a2bb-44e4-a360-2d91411771b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>79084528-2d54-49da-a769-0d7bdb92856f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
 
  
    Select a speed
    
      Slower
      Slow
      Medium
      Fast
      Faster
    
	
    Select a file
    
        TXT file
        PDF file
        DOC file
        Other file
    
 
    Select a number
    
      1
      2
      3
      4
     5
    
	
	 
  
  
  
	Select a Product:
    
		Google
		Yahoo
		Iphone
		Bing
	
	
	Select a Animal:
    
		Cat
		Baby Cat
		Big Baby Cat
		Avatar
	
	
 
</value>
      <webElementGuid>a2880ed9-ea79-4ea4-81ae-30a40060f660</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;HTML6&quot;)/div[@class=&quot;widget-content&quot;]/form[1]</value>
      <webElementGuid>34824687-d44e-4628-ac52-0299d17bdf4b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@action='#']</value>
      <webElementGuid>54561210-5a08-40ab-b892-8c5b5ff7ab4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='HTML6']/div/form</value>
      <webElementGuid>45ddd902-f924-47be-be14-c5c420423e86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select menu'])[1]/following::form[1]</value>
      <webElementGuid>d7fbc9f4-3848-4b71-900c-ab68556e892c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date Picker'])[1]/following::form[1]</value>
      <webElementGuid>a58d9314-48c1-4c82-8a68-a8d11c8033e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/form</value>
      <webElementGuid>a8128d07-4ee6-4759-bc17-6e7430a1188f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[(text() = '
 
  
    Select a speed
    
      Slower
      Slow
      Medium
      Fast
      Faster
    
	
    Select a file
    
        TXT file
        PDF file
        DOC file
        Other file
    
 
    Select a number
    
      1
      2
      3
      4
     5
    
	
	 
  
  
  
	Select a Product:
    
		Google
		Yahoo
		Iphone
		Bing
	
	
	Select a Animal:
    
		Cat
		Baby Cat
		Big Baby Cat
		Avatar
	
	
 
' or . = '
 
  
    Select a speed
    
      Slower
      Slow
      Medium
      Fast
      Faster
    
	
    Select a file
    
        TXT file
        PDF file
        DOC file
        Other file
    
 
    Select a number
    
      1
      2
      3
      4
     5
    
	
	 
  
  
  
	Select a Product:
    
		Google
		Yahoo
		Iphone
		Bing
	
	
	Select a Animal:
    
		Cat
		Baby Cat
		Big Baby Cat
		Avatar
	
	
 
')]</value>
      <webElementGuid>6f71fdc2-4679-4513-829f-f4b07df0e1da</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
