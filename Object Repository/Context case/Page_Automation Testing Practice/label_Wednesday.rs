<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Wednesday</name>
   <tag></tag>
   <elementGuidId>96e8e6f7-6e4e-4dc9-90c0-9aed96ce6f94</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='q15']/table/tbody/tr[4]/td/label</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>b82f1526-0e31-4681-8d00-af57aa0f3825</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>RESULT_CheckBox-8_3</value>
      <webElementGuid>b0b3589b-5e5f-4d70-a55b-de8a481ebf21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Wednesday</value>
      <webElementGuid>93bb2ef3-5bbb-4fe6-a459-0a3a26841258</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;q15&quot;)/table[@class=&quot;inline_grid choices&quot;]/tbody[1]/tr[4]/td[1]/label[1]</value>
      <webElementGuid>0228f790-888b-4785-b0c8-7c887ca76efe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Context case/Page_Automation Testing Practice/iframe_Automation Testing Practice_frame-on_f39fe2 (1)</value>
      <webElementGuid>9e932f5b-14d0-4524-acc8-3350722a100e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='q15']/table/tbody/tr[4]/td/label</value>
      <webElementGuid>073c9893-c9ad-4be8-b216-692be235b4c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tuesday'])[1]/following::label[1]</value>
      <webElementGuid>8ed13a63-0953-4e9f-bd9d-25152f2b5b83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Monday'])[1]/following::label[2]</value>
      <webElementGuid>52a85856-7e96-4606-83b3-cf41b43a7f0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Thursday'])[1]/preceding::label[1]</value>
      <webElementGuid>49f5165a-d162-4f72-b128-21617cae0537</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Friday'])[1]/preceding::label[2]</value>
      <webElementGuid>5feb9e2f-52be-4fae-acce-4fd5402d93c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Wednesday']/parent::*</value>
      <webElementGuid>89761f2a-a7fc-4df2-b63f-c29174a80cbc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td/label</value>
      <webElementGuid>b477bfe3-f431-4af1-b0f6-8f228fe70651</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'Wednesday' or . = 'Wednesday')]</value>
      <webElementGuid>cba94992-267c-4be7-9267-d3d5d4b679ef</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
