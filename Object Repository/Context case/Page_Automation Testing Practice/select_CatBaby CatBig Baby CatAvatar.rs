<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_CatBaby CatBig Baby CatAvatar</name>
   <tag></tag>
   <elementGuidId>bd792a99-b89b-414f-905e-9334d26036ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='animals']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#animals</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>154a7bb2-a1ed-4f97-9bf0-47f54a197a77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>animals</value>
      <webElementGuid>7c18ed19-35ae-4365-94d5-98105242aabe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>animals</value>
      <webElementGuid>26e8a7d5-5a73-4833-89a1-0e4b323967a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		Cat
		Baby Cat
		Big Baby Cat
		Avatar
	</value>
      <webElementGuid>4da10313-6c24-4c28-b3c2-0d00a881d14a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;animals&quot;)</value>
      <webElementGuid>f3098981-bfe0-4146-8863-634c977db383</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='animals']</value>
      <webElementGuid>687d5702-8071-4705-b3ef-6ca71d5e901e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='HTML6']/div/form/fieldset[2]/p/select</value>
      <webElementGuid>90a6cddd-e60b-45aa-8b81-5443a2448ab4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select a Animal:'])[1]/following::select[1]</value>
      <webElementGuid>fe175ea8-6df7-488f-856b-915753e429f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select a Product:'])[1]/following::select[2]</value>
      <webElementGuid>d02099dc-3045-429a-bdea-9a13d8ede850</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Text Labels'])[1]/preceding::select[1]</value>
      <webElementGuid>d903e0dc-ced5-4bd8-8aa9-9337e72b3fa6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Message_12'])[1]/preceding::select[1]</value>
      <webElementGuid>fd87f4d2-c041-4d26-8df8-c0eb1663579b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[2]/p/select</value>
      <webElementGuid>f45fd78b-57c4-4419-8619-266ffd5324e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'animals' and @id = 'animals' and (text() = '
		Cat
		Baby Cat
		Big Baby Cat
		Avatar
	' or . = '
		Cat
		Baby Cat
		Big Baby Cat
		Avatar
	')]</value>
      <webElementGuid>6f4f9b37-3183-49a5-a337-5812a729470f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
