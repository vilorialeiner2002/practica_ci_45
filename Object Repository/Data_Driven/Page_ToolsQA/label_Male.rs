<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Male</name>
   <tag></tag>
   <elementGuidId>455f3e9b-9ee7-4708-ad55-8404d159863a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label.custom-control-label</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='genterWrapper']/div[2]/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>2695e71b-1a7a-4ff4-9e43-766cec7ff39e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>gender-radio-1</value>
      <webElementGuid>8daa4957-d4f4-46f0-bc10-13d467f9b968</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>custom-control-label</value>
      <webElementGuid>1c807284-0d5f-48b3-9134-178a777014fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Male</value>
      <webElementGuid>9cecd683-2d20-4c14-8aac-bf35b3926dfd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;genterWrapper&quot;)/div[@class=&quot;col-md-9 col-sm-12&quot;]/div[@class=&quot;custom-control custom-radio custom-control-inline&quot;]/label[@class=&quot;custom-control-label&quot;]</value>
      <webElementGuid>034628a3-6e80-47f0-9b43-123ec2d88002</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='genterWrapper']/div[2]/div/label</value>
      <webElementGuid>65fffbf6-d9b5-49ff-82b5-0f66df09f16a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gender'])[1]/following::label[1]</value>
      <webElementGuid>a94896ef-aca3-43b8-9218-d1a749de5bcd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::label[1]</value>
      <webElementGuid>5ff5c3e0-503c-43fb-aed9-bf32877b6137</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Female'])[1]/preceding::label[1]</value>
      <webElementGuid>186df16d-2145-4051-9f79-8010bb2ced27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Other'])[1]/preceding::label[2]</value>
      <webElementGuid>c759b883-f6a3-427b-95dd-0df8bd852c86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Male']/parent::*</value>
      <webElementGuid>550131d0-92e1-4942-8ba6-c9632d463744</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div/label</value>
      <webElementGuid>f7b92466-2737-4431-b690-08cc0f3d02d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'Male' or . = 'Male')]</value>
      <webElementGuid>4a2115dc-197f-4e57-9975-a7def49c7d0f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
