import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://crossbrowsertesting.com/freetrial?src=newFooterCTA')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/09_Select/Page_Try A Better Testing Experience For 7 _f4a4c5/input_Work Email_email'), 
    'David@gmail.com')

WebUI.setText(findTestObject('Object Repository/09_Select/Page_Try A Better Testing Experience For 7 _f4a4c5/input_Must be 12 characters_password'), 
    '123456789012')

WebUI.setText(findTestObject('Object Repository/09_Select/Page_Try A Better Testing Experience For 7 _f4a4c5/input_Phone Number is required_phone'), 
    '3001234567')

WebUI.selectOptionByLabel(findTestObject('Object Repository/09_Select/Page_Try A Better Testing Experience For 7 _f4a4c5/select_Select Best MatchStudentDesignerDeve_b45b97'), 
    'QA', false)

WebUI.delay(2)

WebUI.selectOptionByIndex(findTestObject('Object Repository/09_Select/Page_Try A Better Testing Experience For 7 _f4a4c5/select_Select Best MatchStudentDesignerDeve_b45b97'), 
    3, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('09_Select/Page_Try A Better Testing Experience For 7 _f4a4c5/select_Select Best MatchStudentDesignerDeve_b45b97'), 
    'devOps', false)

WebUI.takeScreenshot('C:\\todo_Katalon\\1.jpg')

WebUI.delay(2)

WebUI.selectAllOption(findTestObject('09_Select/Page_Try A Better Testing Experience For 7 _f4a4c5/select_Select Best Match            Live Te_27b76f'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/09_Select/Page_Try A Better Testing Experience For 7 _f4a4c5/a_Create your free account'))

WebUI.delay(5)

WebUI.takeScreenshot('C:\\todo_Katalon\\2.jpg')

WebUI.closeBrowser()

