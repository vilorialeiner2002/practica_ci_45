import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.olimpica.com/?gclid=Cj0KCQiAxoiQBhCRARIsAPsvo-w3XIh4FFCRj4bYSFRhA1VVc-4OFe7xlkz8x1616Cam5VNAtY8Qf3UaAipkEALw_wcB')

WebUI.maximizeWindow()

WebUI.waitForPageLoad(30)

WebUI.waitForElementClickable(findTestObject('14_F_Await/Page_Ofertas Televisores, Aires acondiciona_ee8f56/button_No, gracias'), 
    5)

WebUI.click(findTestObject('Object Repository/14_F_Await/Page_Ofertas Televisores, Aires acondiciona_ee8f56/button_No, gracias'))

WebUI.navigateToUrl('https://www.olimpica.com/login?returnUrl=%2Faccount%23%2Flista-deseos')

WebUI.setText(findTestObject('14_F_Await/Page_Ofertas Televisores, Aires acondicionados y mucho ms/input_Entrar 1'), 'leiner@example.com')

WebUI.click(findTestObject('14_F_Await/Page_Ofertas Televisores, Aires acondicionados y mucho ms/input_Entrar con email 02'))

WebUI.setText(findTestObject('14_F_Await/Page_Ofertas Televisores, Aires acondicionados y mucho ms/input_Entrar con email 02'), 
    '1')

WebUI.setText(findTestObject('14_F_Await/Page_Ofertas Televisores, Aires acondicionados y mucho ms/input_Entrar 1'), '12345')

WebUI.waitForElementVisible(findTestObject('14_F_Await/Page_Ofertas Televisores, Aires acondicionados y mucho ms/span_Entrar'), 
    1)

WebUI.click(findTestObject('14_F_Await/Page_Ofertas Televisores, Aires acondicionados y mucho ms/span_Entrar'))

WebUI.closeBrowser()

