import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.selectOptionByValue(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/select_1      2      3      4     5'), 
    '2', true)

WebUI.click(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/form_Select a speed          Slower      Sl_dddd58'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/select_1      2      3      4     5'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/select_1      2      3      4     5'), 
    '4', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/select_1      2      3      4     5'), 
    '5', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/select_GoogleYahooIphoneBing'), 
    'Yahoo', true)

WebUI.click(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/fieldset_Select a Product    GoogleYahooIph_b9b927'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/select_GoogleYahooIphoneBing'), 
    'Apple', true)

WebUI.click(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/fieldset_Select a Product    GoogleYahooIph_b9b927'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/select_GoogleYahooIphoneBing'), 
    'Microsoft', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/select_CatBaby CatBig Baby CatAvatar'), 
    'babycat', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/select_CatBaby CatBig Baby CatAvatar'), 
    'big baby cat', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Context case/Page_Automation Testing Practice/select_CatBaby CatBig Baby CatAvatar'), 
    'avatar', true)

